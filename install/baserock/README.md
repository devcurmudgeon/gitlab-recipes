# Installing GitLab on Baserock

These are notes for installing GitLab manually onto a Baserock system,
to be used until we automate this.

### Pre-requisites:

* Build and deploy a Baserock system with Ruby and RubyGems, for example
  [this one](http://git.baserock.org/cgi-bin/cgit.cgi/baserock/baserock/definitions.git/tree/web-system.morph?h=baserock/ps/proto-web-system&id=ec715cab2bfe987487c4c9b35d587c1b514e3017). 
  GitLab will be installed by logging into the deployed system via ssh.

* Additional files: ./units/*

```bash
# Based on https://gist.github.com/devcurmudgeon/d4875e31c608d6d0e302 and http://doc.gitlab.com/ce/install/installation.html
# version liw-v2

# First, install bundle
gem install bundler
 
# Setup users needed for GitLab
adduser -D --gecos 'GitLab' git
adduser -D postgres

# PostgreSQL setup
su - postgres
mkdir -p /home/postgres/pgsql/data
 
pg_ctl -D /home/postgres/pgsql/data initdb
pg_ctl -D /home/postgres/pgsql/data -l logfile start
# Login to PostgreSQL
psql -d template1
# Create a user for GitLab.
CREATE USER git;
# Create the GitLab production database & grant all privileges on database
CREATE DATABASE gitlabhq_production OWNER git;
# Quit the database session
\q
exit # end of postgres user stuff

# GitLab setup
su - git

# before doing anything else, check the db setup
psql -d gitlabhq_production
\q

git config --global http.sslVerify false
git config --global user.name "GitLab"
git config --global user.email "gitlab@localhost"
git config --global core.autocrlf input

# Install gitlab-shell
git clone https://gitlab.com/gitlab-org/gitlab-shell.git -b v1.9.3
cd gitlab-shell
cp config.yml.example config.yml
vi config.yml # replace localhost with hostname
./bin/install
cd ..

# Install gitlab
git clone https://gitlab.com/gitlab-org/gitlab-ce.git -b 6-8-stable gitlab
cd gitlab
cp config/gitlab.yml.example config/gitlab.yml
cp config/resque.yml.example config/resque.yml
cp config/database.yml.postgresql config/database.yml
cp config/unicorn.rb.example config/unicorn.rb
cp config/initializers/rack_attack.rb.example config/initializers/rack_attack.rb
 
chmod -R u+rwX log/
chmod -R u+rwX tmp/
chmod o-rwx config/database.yml
 
# Configure stuff
vi config/resque.yml # change redis.example.com to 'baserock' or 'localhost' or 127.0.0.1
vi config/gitlab.yml # fix the hostname

# Run install scripts with bundle
/usr/bin/redis-server & # start the redis server (otherwise setup fails)
bundle install --deployment --without development test mysql aws
bundle exec rake gitlab:setup RAILS_ENV=production
  # the above will ask you to confirm it can overwrite things in db; say yes
bundle exec rake gitlab:env:info RAILS_ENV=production
bundle exec rake assets:precompile RAILS_ENV=production
 
exit # end of git user stuff

mkdir -p /etc/default
cp /home/git/gitlab/lib/support/init.d/gitlab /etc/init.d/gitlab
cp /home/git/gitlab/lib/support/init.d/gitlab.default.example /etc/default/gitlab

# fix nginx config
addgroup nobody
mkdir -p /var/log/nginx # make log directory that nginx expects
vi /home/git/gitlab/lib/support/nginx/gitlab # change server name from YOUR_SERVER_FQDN to hostname, get rid of the gzip code that doesn't work (comment out the line near the end that says gzip on)
vi /usr/conf/nginx.conf # remove server stuff, include /home/git/gitlab/lib/support/nginx/gitlab instead (remember semicolon)

/etc/init.d/gitlab restart
nginx # start the web server
 
# see if it all worked...
su - git
cd gitlab
bundle exec rake gitlab:check RAILS_ENV=production
exit

# now add systemd units so everything runs on startup
# put the ./units/* files into /etc/systemd/system


# edit config/gitlab.yml to set email addresses and then edit
# config/environments/production.rb to set up the mail sending method (assuming
# you want gitlab to send email):
# -  config.action_mailer.delivery_method = :sendmail
# -  # Defaults to:
# -  # # config.action_mailer.sendmail_settings = {
# -  # #   location: '/usr/sbin/sendmail',
# -  # #   arguments: '-i -t'
# -  # # }
# +  config.action_mailer.delivery_method = :smtp
# +  config.action_mailer.smtp_settings = {
# +    address: 'my.mail.server',
# +    port: 25,
# +    domain: 'my.domain'
# +  }
# +  config.action_mailer.default_options = {from: 'overriding@address.if.needed'}
#    config.action_mailer.perform_deliveries = true
#    config.action_mailer.raise_delivery_errors = true

# This is because we do not have /usr/sbin/sendmail or similar on the web-dev
# system right now.

# and finally...
reboot
```

### Troubleshooting

* push over git doesn't work? as root: "passwd git" and type the same password twice